The test contains all the three task from the request. 

The first task is under folder - package com.dimitare.luxoft.problemOne
the second task is under folder - package com.dimitare.luxoft.problemTwo
the third task is under folder - package com.dimitare.luxoft.problemThree;

The whole project is build on the top of maven enviroment. 
The entry point of the project is at com.dimitare.luxoft.App.java

It is like a helper class that can receive commands and redirect them to the correct problem.

Problem 1 posible commands:
-a n i
-a - starts problem one
n - number of elelment i nthe array
i - element from the end that to be displayed

example: -aa 10 2

Problem 2 posible commands:
-b - starts problem two
[n] - aray of positive digist

example: -b 1 2 3 1

Problem 3 posible commands:
-c option [params]
-c - starts problem two
option - command to execute
[params] - params to be passed

example -c add bossId=9 name=John age=48 address=Bourgas




The following commands we executed for the test log file:
	 - Problem 1:
	 	-a 10 2
		-a
		-a 10 1
	- Problem 2:
		-b 1 2 3 1
		-b 1 2 3 9
		-b 
	- Problem 3:
		-c add bossId=9 name=John age=48 address=Bourgas
		-c add bossId=1 name=Mary age=24 address=London
		-c add bossId=1 name=Peter age=29 address=Brighton
		-c add bossId=1 name=Michael age=28 address=Brighton
		-c add name=Sam age=27 address=Warszawa bossId=4
		-c add name=Sam age=35 address=Warszawa bossId=2
		-c add name=Will age=26 address=Krakow bossId=2
		-c add bossId=7 name=Jackie age=25 address=Krakow
		-c add bossId=7 name=Frank age=36 address=Łódź
		-c 
		-c update bossId=1 name=JohnSmith age=29 address=BrightonUK employeeId=1
		-c update bossId=6 name=PeterStone age=27 address=London employeeId=3
		-c update bossId=1 name=Peter age=35 address=LondonUK employeeId=3
		-c
		-c find name=John
		-c find name=JohnSmith
		-c find name=Mary







