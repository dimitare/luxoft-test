package com.dimitare.luxoft.problemThree;

import java.util.ArrayList;
import java.util.List;

public class Company {
	private Node<Employee> root;
	private int numberEmployees = 0;
	

	public int getNumberEmployees() {
		return numberEmployees;
	}
	
	public int getNextEmployeesNumber() {
		return ++numberEmployees;
	}
	
	public Node<Employee> getRoot() {
		return root;
	}
	
	public void setRoot(Node<Employee> root) {
		this.root = root;
	}
	
	Node<Employee> buildCeoNode(int bossEmployeeId, String name, int employeeId, int age, String address) {
		return new Company.Node<Employee>(buildCEO(employeeId, name, age, address), 0);
	}
	
	Node<Employee> buildEmplyeeNode(int level, int bossEmployeeId, String name, int employeeId, int age, String address) {
		return new Node<Employee>(buildRegularEmployee(employeeId, name, age, address), level);
	}
	
	private static Employee buildCEO(int employeeId, String name, int age, String address) {
		EmployeeImpl.Builder builder = new CompanyCEO.Builder(employeeId, name, age, address);
		builder.positionName("CEO");
		return builder.build();
	}
	
	private static Employee buildRegularEmployee(int employeeId, String name, int age, String address) {
		EmployeeImpl.Builder builder = new EmployeeImpl.Builder(employeeId, name, age, address);
		return builder.build();
	}

	public boolean isCeo(Node<Employee> employee) {
		if( employee == null || this.root == null) {
			return false;
		}
		if( this.root.getEmployeeData() instanceof CompanyCEO ) {
			CompanyCEO ceo = (CompanyCEO) this.root.getEmployeeData();
			return ceo.equals(employee.getEmployeeData());
		}
		return false;
	}
	
	class Node<T> {
		 private T employeeData = null;
		 
		 private int level = 0;
		 
		 private List<Node<T>> reports = new ArrayList<Node<T>>();
		 
		 private Node<T> boss = null;
		 
		 public Node(T employeeData, int level) {
			 this.employeeData = employeeData;
		 }
		 
		 public Node<T> addReport(Node<T> report) {
			 report.setBoss(this);
			 this.reports.add(report);
			 return report;
		 }
		 
		 public Node<T> removeReporter(Node<T> report) {
			 report.setBoss(null);
			 this.reports.remove(report);
			 return report;
		 }
			 
		 public void addReports(List<Node<T>> reports) {
			 reports.forEach(each -> each.setBoss(this));
			 this.reports.addAll(reports);
		 }
			 
		 public List<Node<T>> getReports() {
			 return reports;
		 }
			 
		 public T getEmployeeData() {
			 return this.employeeData;
		 }
			 
		 public void setEmployeeData(T employeeData) {
			 this.employeeData = employeeData;
		 }
			 
		 private void setBoss(Node<T> parent) {
			 this.boss = parent;
		 }
			 
		 public Node<T> getBoss() {
			 return boss;
		 }
		 
		 public int getLevel() {
			 return this.level;
		 }
		 
		 public void setLevel(int level) {
			 this.level = level;
		 }
		 
		 public int reporterLevel() {
			 return this.level+1;
		 }

		public void cleanReporters() {
			this.reports.clear();
		}
	}

	public void updateReporter(Node<Employee> oldNode, Node<Employee> newNode) {
		List<Node<Employee>> reports = oldNode.getReports();
		if( reports == null || reports.isEmpty() ) {
			return;
		}
		for (Node<Employee> each : reports) {
			each.setBoss(newNode);
			each.setLevel(newNode.reporterLevel());
		}
		newNode.addReports(reports);
	}
}
