package com.dimitare.luxoft.problemThree;

class EmployeeImpl implements Employee {
	private int employeeId;
	private String name;
	private int age;
	private String address;
	private String positionName;
	
	EmployeeImpl(Builder builder) {
		this.employeeId = builder.employeeId;
		this.name = builder.name;
		this.age = builder.age;
		this.address = builder.address;
		this.positionName = builder.positionName;
	}

	@Override
	public int getEmployeeId() {
		return this.employeeId;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getAge() {
		return this.age;
	}

	@Override
	public String getAddress() {
		return this.address;
	}

	@Override
	public String positionName() {
		return positionName;
	}
	 
	public static class Builder {
		private int employeeId;
		private String name;
		private int age;
		private String address;
		private String positionName = "Regular Employee";
		
		public Builder(int employeeId, String name, int age, String address) {
			this.employeeId = employeeId;
			this.name = name;
			this.age = age;
			this.address = address;
		}
		
		public Employee build() {
			return new EmployeeImpl(this);
		}
		
		public Builder positionName(String positionName) {
			this.positionName = positionName;
			return this;
		}
	}
}
