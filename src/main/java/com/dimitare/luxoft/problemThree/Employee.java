package com.dimitare.luxoft.problemThree;

public interface Employee {
	String getName();
	int getEmployeeId();
	int getAge();
	String getAddress();
	String positionName();
}
