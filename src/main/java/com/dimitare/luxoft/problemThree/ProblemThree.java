package com.dimitare.luxoft.problemThree;

import java.util.HashMap;
import java.util.Map;

import com.dimitare.luxoft.Task;
import com.dimitare.luxoft.utils.TextUtils;
import com.dimitare.luxoft.problemThree.Company.Node;

public class ProblemThree implements Task {
	private static final String ADD_PARAM = "add";
	private static final String UPDATE_PARAM = "update";
	private static final String FIND_PARAM = "find";
	
	private Company company;
	
	public ProblemThree() {
		this.company = new Company();
	}

	public String execute(String... args) {
		return processCommand(args);
	}
	
	private String processCommand(String... args) {
		if( args.length < 1 ) {
			throw new IllegalArgumentException("Not enough params. Please provide bossEmployeeId, String name, employeeId(int), age(int), address(String)");
		}
		String command = args[0];
		if( TextUtils.isEmpty(command) ) {
			throw new IllegalArgumentException("command is enmpty");
		}
		if(FIND_PARAM.equals(command)) {
			String[] array = new String[args.length-1];
      		System.arraycopy(args, 1, array, 0, args.length-1);
      		return executeFindCommand(array);
		}
		if( args.length > 4 ) {
			if(ADD_PARAM.equals(command)) {
				String[] array = new String[args.length-1];
	      		System.arraycopy(args, 1, array, 0, args.length-1);
				return executeAddCommand(array);
			}
			else if(UPDATE_PARAM.equals(command)) {
				String[] array = new String[args.length-1];
	      		System.arraycopy(args, 1, array, 0, args.length-1);
	      		return executeUpdateCommand(array);
			}
		}
		return "Command \"" + command + "\" not available";
	}
	
	private String executeFindCommand(String[] array) {
		Map<String, String> params = parseAddParams(array);
		String name = getValue(params, "name");
		if( name == null ) {
			return "Wrong params. Please provide query param!";
		}
		return findEmployee(name);
	}
	
	private String findEmployee(String name) {
		Node<Employee> root = this.company.getRoot();
		if( root == null ) {
			return noEmployeesYetMessage();
		}
		System.out.println("Company record starts\n---------------------------");
		printFindResult(this.company.getRoot(), name);
		return "---------------------------\nCompany record finished";
	}
	
	private static <T> void printFindResult(Node<Employee> node, String name) {
		if( name.equals(node.getEmployeeData().getName())) {
			System.out.println(emplyeeToString(node, " "));
		}
		node.getReports().forEach(each ->  printFindResult(each, name));
	}
	
	private String executeUpdateCommand(String[] array) {
		Map<String, String> params = parseAddParams(array);
		String value = getValue(params, "employeeId");
		int employeeId = parseIntParam(value);
		String bossIdString = getValue(params, "bossId");
		int bossId = parseIntParam(bossIdString);
		String name = getValue(params, "name");
		String ageValue = getValue(params, "age");
		int age = parseIntParam(ageValue);
		String address = getValue(params, "address");
		ensureUpdateValidParams(name, age, address);
		return updateEmployee(bossId, name, employeeId, age, address);
	}
	
	private void ensureUpdateValidParams(String name, int age, String address) {
		if( TextUtils.isEmpty(name) || (age < 18 && age > 100) || TextUtils.isEmpty(address) ) {
			throw new IllegalArgumentException("parameters passed are illigal. Please provide size of array");
		}
	}

	private String executeAddCommand(String[] array) {
		Map<String, String> params = parseAddParams(array);
		int employeeId = company.getNextEmployeesNumber();
		int bossId = employeeId;
		if( this.company.getRoot() != null ) {
			String value = getValue(params, "bossId");
			bossId = parseIntParam(value);
		}
		String name = getValue(params, "name");
		String value = getValue(params, "age");
		int age = parseIntParam(value);
		String address = getValue(params, "address");
		ensureAddValidParams(bossId, name, employeeId, age, address);
		return addEmployee(bossId, name, employeeId, age, address);
	}
	
	private void ensureAddValidParams(int bossId, String name, int employeeId, int age, String address) {
		if( bossId < 1 || TextUtils.isEmpty(name) && employeeId < 1 || ( age < 18 && age > 100  ) || TextUtils.isEmpty(address) ) {
			throw new IllegalArgumentException("parameters passed are illigal. Please provide size of array");
		}
	}

	private String getValue(Map<String, String> params, String key) {
		if( params.containsKey(key)) {
			return params.get(key);
		}
		return null;
	}
	
	private int parseIntParam(String param) {
		if( TextUtils.isEmpty(param)) {
			throw new IllegalArgumentException("parameter " + param + " pass is illigal. Please provide size of array");
		}
		try {
			return Integer.parseInt(param);
		}catch( NumberFormatException ex ) {
			throw new IllegalArgumentException("parameter " + param + " pass is illigal. Please provide size of array");
		}
	}
	
	private Map<String, String> parseAddParams(String[] array) {
		Map<String, String> map = new HashMap<String, String>();
		for( String s : array ) {
			String[] tmp = s.split("=");
			if( tmp.length != 2 ) {
				throw new IllegalArgumentException("wraong size of params!!!");
			}
			map.put(tmp[0], tmp[1]);
		}
		return map;
	}
	
	private String updateEmployee(int bossId, String name, int employeeId, int age, String address) {
		Node<Employee> root = this.company.getRoot();
		if( root == null ) {
			return noEmployeesYetMessage();
		}
		Node<Employee> employee = findBossById(root, employeeId);
		if( employee == null ) {
			return "Emoloyee with id:" + employeeId + " does not exists! Please add it first";
		} 
		if( this.company.isCeo(employee) ) { 
			Node<Employee> newCeo = this.company.buildCeoNode(employee.getEmployeeData().getEmployeeId(), name, employee.getEmployeeData().getEmployeeId(), age, address);
			this.company.updateReporter(employee, newCeo);
			this.company.setRoot(newCeo);
			employee.cleanReporters();
			employee.setEmployeeData(null);
		}
		else {
			Node<Employee> currentBoss = findBossById(root, employee.getBoss().getEmployeeData().getEmployeeId());
			if( currentBoss == null ) {
				return "Emoloyee with id:" + employeeId + " has issues. Please fix it first";
			}
			Node<Employee> boss = findBossById(root, bossId);
			if( boss == null ) {
				return "Emoloyee with id:" + employeeId + " has issues. Please fix it first";
			}
			Node<Employee> newEmployee = this.company.buildEmplyeeNode(boss.reporterLevel(), bossId, name, employeeId, age, address);
			currentBoss.removeReporter(employee);
			this.company.updateReporter(employee, newEmployee);
			employee.cleanReporters();
			employee.setEmployeeData(null);
			boss.addReport(newEmployee);
		}
		return "Employee name:" + name + " employeeId:" +  employeeId + " updated successfully";
	}

	private String addEmployee(int bossEmployeeId, String name, int employeeId, int age, String address) {
		Node<Employee> root = this.company.getRoot();
		if( root == null ) {
			this.company.setRoot(this.company.buildCeoNode(bossEmployeeId, name, employeeId, age, address));
			return "Adding CEO success";
		}
		else {
			Node<Employee> boss = findBossById(root, bossEmployeeId);
			if( boss == null ) {
				return "bossId:" + bossEmployeeId + " for this employee does not exists!!!";
			}
			Node<Employee> employee = this.company.buildEmplyeeNode(boss.reporterLevel(), bossEmployeeId, name, employeeId, age, address);
			boss.addReport(employee);
			return "Adding employee employeeId: " + employeeId + " name: " + name + " bossId:" + boss.getEmployeeData().getEmployeeId();
		}
	}
	
	private Node<Employee> findBossById(Node<Employee> root, int employeeId){
		if( root == null ) {
			return null;
		}		
		Employee employee = root.getEmployeeData();
		if( employee.getEmployeeId() == employeeId) {
			return root;
		}
		Node<Employee> node = null;
		for (Node<Employee> employeeNode : root.getReports()) {
			node = findBossById(employeeNode, employeeId);
			if( node != null ) {
				return node;
			}
		}
		return node;
	}
	
	private String noEmployeesYetMessage() {
		return "---------------------------\nCompany has no employees yet! \n\nPlease add CEO first\n---------------------------";
	}

	public String print() {
		Node<Employee> root = this.company.getRoot();
		if( root == null ) {
			return noEmployeesYetMessage();
		}
		System.out.println("Company record starts\n---------------------------");
		printTree(this.company.getRoot(), " ");
		return "---------------------------\nCompany record finished";
	}
	
	private static <T> void printTree(Node<Employee> node, String prefix) {
		//emplyeeToString(node, prefix);
		System.out.println(emplyeeToString(node, prefix));
		node.getReports().forEach(each ->  printTree(each, prefix + prefix));
	}
	
	private static String emplyeeToString(Node<Employee> node, String prefix) {
		Employee e = node.getEmployeeData();
		Node<Employee> boss = node.getBoss();
		StringBuilder sb = new StringBuilder();
		sb.append(prefix + e.getName());
		sb.append(" age:" + e.getAge());
		sb.append(" address:" + e.getAddress());
		sb.append(" emplyeeId:" + e.getEmployeeId());
		if( boss == null ) {
			sb.append(" position:\"" + e.positionName() + "\"");
		}
		else {
			sb.append(" bossName:" + boss.getEmployeeData().getName() +" bossId:" + boss.getEmployeeData().getEmployeeId());
			sb.append(" position:\"" + e.positionName() + "\"");
		}
		return sb.toString();
	}
}
