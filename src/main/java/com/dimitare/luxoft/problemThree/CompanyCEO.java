package com.dimitare.luxoft.problemThree;

public class CompanyCEO extends EmployeeImpl {
	CompanyCEO(Builder builder) {
		super(builder);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
		result = prime * result + getAge();
		result = prime * result + getEmployeeId();
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((positionName() == null) ? 0 : positionName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EmployeeImpl other = (EmployeeImpl) obj;
		if (getAddress() == null) {
			if (other.getAddress() != null) {
				return false;
			}
		} else if (!getAddress().equals(other.getAddress())) {
			return false;
		}
		if (getAge() != other.getAge()) {
			return false;
		}
		if (getEmployeeId() != other.getEmployeeId()) {
			return false;
		}
		if (getName() == null) {
			if (other.getName() != null) {
				return false;
			}
		} else if (!getName().equals(other.getName())) {
			return false;
		}
		if (positionName() == null) {
			if (other.positionName() != null) {
				return false;
			}
		} else if (!positionName().equals(other.positionName())) {
			return false;
		}
		return true;
	}
	 
	public static class Builder extends EmployeeImpl.Builder {
		public Builder(int employeeId, String name, int age, String address) {
			super(employeeId, name, age, address);
		}
		
		public Employee build() {
			return new CompanyCEO(this);
		}
	}
}
