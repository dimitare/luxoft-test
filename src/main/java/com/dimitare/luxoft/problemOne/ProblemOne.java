package com.dimitare.luxoft.problemOne;

import java.util.LinkedList;

import com.dimitare.luxoft.Task;

public class ProblemOne implements Task {
	private LinkedList<Integer> list = null;
	private int node = 0;
	private int searchedIndex = -1;
	
	public String execute(String... args) {
		if( args.length != 2 ) {
			throw new IllegalArgumentException("parameter " + args + " pass is illigal. Please provide size of array");
		}
		int n = 0;
		resetSearchedIndex();
		try {
			n = Integer.parseInt(args[0]);
			searchedIndex = Integer.parseInt(args[1]);
		}catch( NumberFormatException ex ) {
			throw new IllegalArgumentException("parameter " + n + " pass is illigal. Please provide size of array");
		}
		if( n < 1 || searchedIndex < 1 || n < searchedIndex ) {
			throw new IllegalArgumentException("parameter " + n + " pass is illigal. Please provide size of array");
		}
		resetLinkedList(n);
		for( int i = 0; i < this.list.size(); i++ ) {
			node = i - searchedIndex;
		}
		return printResult();
	}

	public String print() {
		return printResult();
	}
	
	private String printResult() {
		return "Problem 1 - size of the lisnked list: " + this.list.size() + " searched elem from the end: " + searchedIndex + "\nRESULT: " + list.get(node); 
	}
	
	private void resetSearchedIndex() {
		this.searchedIndex = -1;
	}
	
	private void resetLinkedList(int size) {
		list = new LinkedList<Integer>();
		for( int i = 0; i < size; i++) {
			list.add( i + 1 );
		}
	}
}
