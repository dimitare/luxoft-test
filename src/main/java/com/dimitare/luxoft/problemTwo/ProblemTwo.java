package com.dimitare.luxoft.problemTwo;

import java.util.Vector;

import com.dimitare.luxoft.Task;

public class ProblemTwo implements Task {
	private Vector<Integer> vector = null;
	private int[] roberCalc = null;
	
	public String execute(String... args) {
		if( args.length < 1 ) {
			throw new IllegalArgumentException("parameter " + args + " pass is illigal. Please provide size of array");
		}
		resetVector();
		resetRoberCalc(args.length);
		initVector(args);
		maximizeBobery();
		return printResult();
	}
	
	private void maximizeBobery() {
		if( this.vector.size() == 1 ) {
			roberCalc[0] = 0;
			return;
		}
		roberCalc[0] = this.vector.get(0);
		roberCalc[1] = Math.max(this.vector.get(0), this.vector.get(1));
		
		for( int i = 2; i < this.vector.size(); i++ ) {
			roberCalc[i] = Math.max(roberCalc[i-2] + this.vector.get(i), roberCalc[i-1]);
		}
	}

	public String print() {
		return printResult();
	}
	
	private String printResult() {
		StringBuilder sb = new StringBuilder();
		sb.append("Problem 2 elems: [");
		for( int elem : this.vector ) {
			sb.append(elem);
			sb.append(",");
		}
		if( sb.length() > 0 ) {
			sb.setLength( sb.length() - 1 );
		}
		sb.append("]");
		sb.append("\nRESULT: ");
		sb.append(roberCalc[this.vector.size()-1]);
		return sb.toString(); 
	}
	
	private void resetRoberCalc(int size) {
		this.roberCalc = new int[size];
	}
	
	private void resetVector() {
		this.vector = new Vector<Integer>();
	}
	
	private void initVector(String[] values) {
		for( String elem : values) {
			vector.add(nextElement(elem));
		}
	}
	
	private int nextElement(String elem) {
		int n = -1;
		try {
			n = Integer.parseInt(elem);
		}catch( NumberFormatException ex ) {
			throw new IllegalArgumentException("parameter " + n + " pass is illigal. Please provide size of array");
		}
		return n;
	}
}
