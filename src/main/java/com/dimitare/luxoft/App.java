package com.dimitare.luxoft;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import com.dimitare.luxoft.utils.TextUtils;

/**
 * Luxoft Test
 *
 */
public class App 
{
	public static final String HELPER_PARAM = "-h"; 
	public static final String QUIT_PARAM = "-q"; 
	public static final String PROBLEM_ONE_PARAM = "-a";
	public static final String PROBLEM_TWO_PARAM = "-b";
	public static final String PROBLEM_THREE_PARAM = "-c"; 
	
    private static PrintWriter output = new PrintWriter(new OutputStreamWriter(System.out));
    private static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    
    // entry point of the project
    public static void main( String[] args )
    {
        output.println();
        output.println("Welcome to Luxoft Test");
        output.println();
        printHelpMeesage();
        output.flush();
        
        while(true) {
            try {
            	// Always print welcome message once enter the project
                output.println("\n\nWaiting for input...\n");
                output.flush();
				String line = input.readLine();
				String[] params = parseInput(line);
				if( params == null ) {
					throw new IllegalArgumentException("parameter " + line + " pass is illigal. Please provide size of array");
				}
				processInput(params);
			} catch (Exception e) {
				output.println("Exception occured: " + e);
				printHelpMeesage();
	            output.flush();
			}
        }
    }
    
    private static String[] parseInput(String line) {
    	if( TextUtils.isEmpty(line) ) {
    		return null;
    	}
    	return line.split(" ");
    }
    
    private static void processInput(String[] params) {
      if( params.length < 1 ) {
          output.println("Wrong numbers of params");
          printHelpMeesage();
          output.println("Terminating");
          output.flush();
          return;
      }
      String argOne = params[0];
      if( TextUtils.isEmpty(argOne) ) {
          printHelpMeesage();
          output.flush ();
          return;
      }
      if( argOne.equals(QUIT_PARAM) ) {
          	output.println("Terminating");
          	output.flush();
    	  	System.exit(0);
    		return;
      }
      // enter the Problem 1
      if( argOne.equals(PROBLEM_ONE_PARAM) ) {
    	  	run(params, TaskManager.PROBLEM_ONE);
      		return;
      }
      // enter the Problem 2
      if( argOne.equals(PROBLEM_TWO_PARAM) ) {
    	  	run(params, TaskManager.PROBLEM_TWO);
  			return;
      }
      // enter the Problem 3
      if( argOne.equals(PROBLEM_THREE_PARAM) ) {
    	  	run(params, TaskManager.PROBLEM_THREE);
  			return;
      }
      printHelpMeesage();
      output.flush();
	}
    
    private static void run(String[] params, String command) {
		Task task = TaskManager.getInstance().findTask(command);
		if( params.length > 2 ) {
			String[] array = new String[params.length-1];
			System.arraycopy(params, 1, array, 0, params.length-1);
			output.println(task.execute(array));
		}else {
			output.println(task.print());
		}
		output.flush();
		return;
    }

	// print helper message, how the app would be used
	private static String printHelpMeesage() {
        output.println("Usage: ");
        output.println("   " + HELPER_PARAM + "		help");
        output.println("   " + QUIT_PARAM + "		quit");
        output.println("   -a		execute Problem 1");
        output.println("   -b		execute Problem 2");
        output.println("   -c		execute Problem 3");
    	return null;
    }
}
