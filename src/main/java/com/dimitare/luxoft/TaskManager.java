package com.dimitare.luxoft;

import java.util.HashMap;
import java.util.Map;

import com.dimitare.luxoft.problemOne.ProblemOne;
import com.dimitare.luxoft.problemThree.ProblemThree;
import com.dimitare.luxoft.problemTwo.ProblemTwo;

public class TaskManager {
	private Map<String,Task> _tasks = new HashMap<String,Task>();
	public static final String PROBLEM_ONE = "a"; 
	public static final String PROBLEM_TWO = "b"; 
	public static final String PROBLEM_THREE = "c"; 
	private static TaskManager instance;
	
	public static TaskManager getInstance() {
		if( instance == null ) {
			instance = new TaskManager();
		}
		return instance;
	}
	
	private TaskManager() {
		_tasks.put(PROBLEM_ONE, new ProblemOne());
		_tasks.put(PROBLEM_TWO, new ProblemTwo());
		_tasks.put(PROBLEM_THREE, new ProblemThree());
	}
	
	Task findTask(String task) {
		return _tasks.get(task);
	}
}
