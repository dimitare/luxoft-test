package com.dimitare.luxoft;

public interface Task {
	String execute(String ...args);
	String print();
}
